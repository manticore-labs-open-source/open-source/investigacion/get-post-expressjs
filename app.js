'use strict'

var express = require('express');
var bodyParser = require("body-parser");
var app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get(
    '/',
    (request, response) => {
        response.send('pagina Principal')
    }
);

app.post('/', function (req, res) {
    var user_name = req.body.user;
    var password = req.body.password;
    console.log("User name = " + user_name + ", password is " + password);
    res.end("yes");
});


app.listen(
    3000,
    () => {
        console.log('Servidor corriendo')
    }
)