# Hacer peticiones Get y Post en expressjs

### ¿Que es expressjs?
**Express** es una infraestructura de aplicaciones web Node.js mínima y flexible que proporciona un conjunto sólido de características para las aplicaciones web y móviles.

Todo el código descrito puede encontrarse [aquí](https://gitlab.com/manticore-labs/open-source/investigacion/get-post-expressjs).

Para instalar express primero debemos iniciar un proyecto npm con el siguiente comando en la terminal.

```
npm init
```

al correr el comando se deben llenar los siguientes campos.

![Campos a llenar en npm](./imagenes/npm-campos.png)

al final escribimos yes y se creará el proyecto npm, y tendremos un archivo package.json donde se guardará toda la información del proyecto.

Ahora podemos instalar express mediante el comando.

```
sudo npm install express --save
```

Esperamos a que la instalación termine se nos creará una carpeta **node_modules** si estas trabajando en un repositorio de git es indispensable que añadas esta carpeta al gitignore.

También instalaremos *body-parser* para poder manejar los parametros enviados mediante peticiones post.
Para instalar body parser utilizaremos el siguiente comando.

```
sudo npm install --save body-parser
```

### Ahora que tenemos instalado express podemos continuar.

Importamos las librerías necesarias necesarias.
``` JavaScript
var express = require('express');
var bodyParser = require("body-parser");
``` 
Iniciamos el código del servidor

``` JavaScript
var app = express();
```
Declaramos la forma en la que el servidor recuperará los body params de la petición post.

``` JavaScript
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
```
El método get se realiza de la siguiente forma, el método recibe como primer parámetro la ruta del GET y como segundo parámetro un callback dónde devuelve una respuesta, generalmente una **pagina web**. Pero en este caso solo enviará un string.
``` JavaScript
app.get(
    '/',
    (request, response) => {
        response.send('pagina Principal')
    }
);
```

El método post se realiza de la siguiente forma, el método recibe como primer parámetro la ruta del POST y como segundo parámetro un callback dónde se mapean los body params, se aplica la lógica necesaria propia de backend y se finaliza mandando un mensaje al emisor del POST.
``` JavaScript
app.post('/', function (req, res) {
    var user_name = req.body.user;
    var password = req.body.password;
    console.log("User name = " + user_name + ", password is " + password);
    res.end("yes");
});
```
Finalmente se levanta el servidor con el metodo **listen**, este método recibe como primer parámentro el puerto en el que se levantará el servidor, y como segundo parámetro recibe un callback que se ejecutará antes de levantar el servidor.

``` JavaScript
app.listen(
    3000,
    () => {
        console.log('Servidor corriendo')
    }
)
```

El comando para levantar el servidor es:
```
node nombre-archivo.js
```

### ***Salidas***

### GET
![Imagen get](./imagenes/get.png)

### POST
Hacer un método post desde **postman**
![Imagen post](./imagenes/post.png)

### Consola
Imagen cuando se levanta el servidor.
![Imagen Server 1](./imagenes/server1.png)

Imagen luego de ejecutar el método POST
![Imagen Server 1](./imagenes/server2.png)

<a href="https://twitter.com/atpsito" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://3.bp.blogspot.com/-E4jytrbmLbY/XCrI2Xd_hUI/AAAAAAAAIAo/qXt-bJg1UpMZmTjCJymxWEOGXWEQ2mv3ACLcBGAs/s1600/twitter.png" title="Sígueme en Twitter"/> @atpsito </a><br>
<a href="https://www.linkedin.com/in/alexander-tigselema-ba1443124/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Alexander Tigselema</a><br>
<a href="https://www.instagram.com/atpsito" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> atpsito</a><br>